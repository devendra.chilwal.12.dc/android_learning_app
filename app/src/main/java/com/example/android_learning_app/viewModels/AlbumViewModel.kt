package com.example.android_learning_app.viewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.android_learning_app.repository.AlbumRepository
import com.example.android_learning_app.response.AlbumData

class AlbumViewModel(private val albumRepository: AlbumRepository): ViewModel() {

    fun getAlbum(): LiveData<AlbumData> {
        return albumRepository.getAlbum()
    }

}