package com.example.android_learning_app.repository

import com.example.android_learning_app.roomDb.Employee
import com.example.android_learning_app.roomDb.EmployeeDao

class DbRepository(private val employeeDao: EmployeeDao) {

        suspend fun insertEmployee(employee: Employee){
            employeeDao.insertEmployee(employee)
        }

        var getAllEmployee = employeeDao.getAllEmployee()

      suspend fun deleteSingleRecord(employee: Employee){
          employeeDao.deleteSingleRecord(employee)
      }

    suspend fun updateRecord(employee: Employee){
        employeeDao.updateRecord(employee)
    }

    suspend fun deleteAllRecord(){
        employeeDao.deleteAllRecords()
    }



}