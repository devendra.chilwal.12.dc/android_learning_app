package com.example.android_learning_app.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.android_learning_app.api.RetrofitClient
import com.example.android_learning_app.response.Album
import com.example.android_learning_app.response.AlbumData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AlbumRepository {
    val TAG = "AlbumRepository"
    var albumDataList: MutableLiveData<AlbumData> = MutableLiveData()

    fun getAlbum(): LiveData<AlbumData>{
        val call = RetrofitClient.apiInterface.getAlbum()
        call.enqueue(object: Callback<AlbumData>{
            override fun onResponse(call: Call<AlbumData>, response: Response<AlbumData>) {
                    if (response.isSuccessful){
                       val responseData =  response.body()
                        albumDataList.postValue(responseData!!)
                        Log.d(TAG, "onResponse: $responseData")
                    }
            }

            override fun onFailure(call: Call<AlbumData>, t: Throwable) {
                Log.d(TAG, "onFailure: ${t.message}")
            }

        })
        return albumDataList!!
    }
}