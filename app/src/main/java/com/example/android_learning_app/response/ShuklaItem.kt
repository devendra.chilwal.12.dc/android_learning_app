package com.example.android_learning_app.response

data class ShuklaItem(
    val id: Int,
    val title: String,
    val userId: Int
)