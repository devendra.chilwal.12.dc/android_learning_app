package com.example.android_learning_app.response

import com.google.gson.annotations.SerializedName

data class Album(
    @SerializedName("userId")
    val userID: Int,
    @SerializedName("id")
    val ID: Int,
    val title: String
)
