package com.example.android_learning_app.roomDb

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase

@Database(entities = [Employee::class], version = 2) //prevVersion  = 1
abstract class MyDatabase(): RoomDatabase() {
    abstract fun employeeDao(): EmployeeDao

    companion object{
        val migration_1_2 =  object: Migration(1, 2){
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("ALTER TABLE Employee ADD COLUMN photo TEXT NOT NULL DEFAULT ''")
            }

        }
        @Volatile
        var INSTANCE: MyDatabase? = null
        fun getInstance(context: Context): MyDatabase{
            var instance = INSTANCE
            if (INSTANCE == null){
                synchronized(this){
                    instance = Room
                        .databaseBuilder(context, MyDatabase::class.java, "empDb")
                        .addMigrations(migration_1_2)
                        .build()
                    INSTANCE = instance
                }
            }
            return INSTANCE!!
        }
    }


}