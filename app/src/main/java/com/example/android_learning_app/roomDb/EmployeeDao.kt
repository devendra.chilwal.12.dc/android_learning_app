package com.example.android_learning_app.roomDb

import androidx.lifecycle.LiveData
import androidx.room.*
import androidx.room.OnConflictStrategy.Companion.REPLACE

@Dao
interface EmployeeDao {
    @Insert(onConflict = REPLACE)
    suspend fun insertEmployee(vararg employee: Employee)

    @Query("SELECT * FROM Employee")
    fun getAllEmployee(): LiveData<List<Employee>>

    @Delete
    suspend fun deleteSingleRecord(employee: Employee)

    @Update
    suspend fun updateRecord(employee: Employee)


    @Query("DELETE FROM Employee")
    suspend fun deleteAllRecords()


}