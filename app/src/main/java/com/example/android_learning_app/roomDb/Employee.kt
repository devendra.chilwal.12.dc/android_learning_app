package com.example.android_learning_app.roomDb

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Employee (
    @PrimaryKey(autoGenerate = true) var srNo: Int,
    @ColumnInfo("firstName") var fName: String,
    var lName: String,
    var phone: String,
    var photo: String
    )