package com.example.android_learning_app.Activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.example.android_learning_app.R
import com.example.android_learning_app.databinding.ActivityDeleteOrUpdateBinding
import com.example.android_learning_app.factory.DbFactory
import com.example.android_learning_app.model.DbViewModel
import com.example.android_learning_app.repository.DbRepository
import com.example.android_learning_app.roomDb.Employee
import com.example.android_learning_app.roomDb.MyDatabase
import com.example.androidconcept_tp_5.util.Keys
import com.google.gson.Gson
import kotlinx.coroutines.CoroutineScope

class DeleteOrUpdateActivity : AppCompatActivity(), View.OnClickListener {
    lateinit var binding: ActivityDeleteOrUpdateBinding
    lateinit var factory: DbFactory
    lateinit var viewModel: DbViewModel
    lateinit var employee: Employee

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_delete_or_update)
        factory = DbFactory(DbRepository(MyDatabase.getInstance(this).employeeDao()))
        viewModel = ViewModelProvider(this, factory)[DbViewModel::class.java]

        val tempIntent = intent
        employee = Gson().fromJson(tempIntent.getStringExtra(Keys.EMPLOYEE), Employee::class.java)

        binding.etFname.setText(employee.fName)
        binding.etLname.setText(employee.lName)
        binding.etPhone.setText(employee.phone)

        binding.btnDelete.setOnClickListener(this)
        binding.btnUpdate.setOnClickListener(this)
        binding.btnDeleteAll.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        when(view?.id){
            R.id.btn_delete ->{
                viewModel.deleteSingleRecord(employee)
                finish()
            }

            R.id.btn_update ->{

                viewModel.updateRecord(Employee(employee.srNo, binding.tilFname.editText?.text.toString(),binding.tilLname.editText?.text.toString(), binding.tilPhone.editText?.text.toString(), ""))
                finish()
            }

            R.id.btn_delete_all ->{

                viewModel.deleteAllRecord()
                finish()
            }
        }
    }
}