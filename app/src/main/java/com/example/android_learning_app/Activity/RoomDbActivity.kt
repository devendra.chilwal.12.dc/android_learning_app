package com.example.android_learning_app.Activity

import android.R.attr.bitmap
import android.app.ActionBar
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.MediaStore
import android.util.Base64
import android.util.Log
import android.view.ContextMenu
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.android_learning_app.R
import com.example.android_learning_app.adapters.MyRecyclerViewAdapterNew
import com.example.android_learning_app.databinding.ActivityRoomDbBinding
import com.example.android_learning_app.databinding.LayoutCreateDataBinding
import com.example.android_learning_app.factory.DbFactory
import com.example.android_learning_app.interfaces.OnItemClickListenerNew
import com.example.android_learning_app.model.DbViewModel
import com.example.android_learning_app.repository.DbRepository
import com.example.android_learning_app.roomDb.Employee
import com.example.android_learning_app.roomDb.MyDatabase
import com.example.androidconcept_tp_5.util.Keys
import com.google.gson.Gson
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.ByteArrayOutputStream


class RoomDbActivity : AppCompatActivity(), View.OnClickListener {
    lateinit var binding: ActivityRoomDbBinding
    lateinit var factory: DbFactory
    lateinit var viewModel: DbViewModel
    var myAdapter: MyRecyclerViewAdapterNew? = null
    lateinit var layoutCustomBinding: LayoutCreateDataBinding
    var base64Image: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_room_db)
        factory = DbFactory(DbRepository(MyDatabase.getInstance(this).employeeDao()))
        viewModel = ViewModelProvider(this, factory)[DbViewModel::class.java]
        binding.recyclerView.layoutManager = GridLayoutManager(this, 2)
        binding.lifecycleOwner = this
        binding.recyclerView.layoutManager = LinearLayoutManager(this)
//        myAdapter = MyRecyclerViewAdapterNew(listOfEmployee, this)

        viewModel.getAllEmployee.observe(this, Observer {
//            Log.d("Employee", "onCreate: $it")
//            listOfEmployee = it
            myAdapter = MyRecyclerViewAdapterNew(it, this, object: OnItemClickListenerNew{
                override fun onItemClick(position: Int, employee: Employee) {
                    var intent = Intent(this@RoomDbActivity, DeleteOrUpdateActivity::class.java)
                    intent.putExtra(Keys.EMPLOYEE, Gson().toJson(employee))
                    startActivity(intent)

                }

            })
            binding.recyclerView.adapter = myAdapter
            myAdapter?.notifyDataSetChanged()

        })

        binding.btnCreateData.setOnClickListener(this)

        registerForContextMenu(binding.recyclerView)

    }

    override fun onClick(p0: View?) {
        layoutCustomBinding = LayoutCreateDataBinding.inflate(layoutInflater)
        val dialog = Dialog(this)
        dialog.setContentView(layoutCustomBinding.root)
        dialog.setCancelable(false)
        val windowManager = WindowManager.LayoutParams()
        windowManager.width = ActionBar.LayoutParams.MATCH_PARENT
        windowManager.height = ActionBar.LayoutParams.WRAP_CONTENT
        dialog.window?.attributes = windowManager
        dialog.show()

        layoutCustomBinding.imageView.setOnClickListener {
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED){
                val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                startActivityForResult(intent, 1002)
            }else{
                ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.CAMERA), 1001)
            }
        }


        layoutCustomBinding.btnSubmit.setOnClickListener {
            CoroutineScope(Dispatchers.IO).launch {
                if (base64Image.isNotEmpty()) {
                    viewModel.insertEmployee(
                        Employee(
                            0,
                            layoutCustomBinding.tilFname.editText?.text.toString(),
                            layoutCustomBinding.tilLname.editText?.text.toString(),
                            layoutCustomBinding.tilPhone.editText?.text.toString(),
                            base64Image
                        )
                    )
                    withContext(Dispatchers.Main){
                        Toast.makeText(this@RoomDbActivity, "Inserted Succefully.", Toast.LENGTH_SHORT).show()
                    }
                }else{
                    withContext(Dispatchers.Main){
                        Toast.makeText(this@RoomDbActivity, "Something went wrong.", Toast.LENGTH_SHORT).show()
                    }
                }


            }
            dialog.dismiss()
        }
    }

    override fun onCreateContextMenu(
        menu: ContextMenu?,
        v: View?,
        menuInfo: ContextMenu.ContextMenuInfo?
    ) {
        super.onCreateContextMenu(menu, v, menuInfo)
        menuInflater.inflate(R.menu.options_menu, menu)
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 1001){
            if(grantResults.isNotEmpty() && permissions[0].equals(PackageManager.PERMISSION_GRANTED)){

            }else{
                Toast.makeText(this, "Please give permission", Toast.LENGTH_SHORT).show()
            }

        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1002 && resultCode == RESULT_OK){
            var bitmp: Bitmap = data?.extras?.get("data") as Bitmap
            layoutCustomBinding.imageView.setImageBitmap(bitmp)
            base64Image = convertBitmapToBase64(bitmp)

            Log.d("TAG", "onActivityResult: $base64Image")

        }else{

        }
    }

    private fun convertBitmapToBase64(bitmap: Bitmap): String{
        val byteArrayOutputStream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream)
        val byteArray: ByteArray = byteArrayOutputStream.toByteArray()
        return Base64.encodeToString(byteArray, Base64.DEFAULT)
    }


}