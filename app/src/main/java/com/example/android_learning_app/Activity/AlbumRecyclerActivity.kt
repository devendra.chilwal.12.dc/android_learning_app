package com.example.android_learning_app.Activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.android_learning_app.R
import com.example.android_learning_app.adapters.MyAlbumRecyclerViewAdapter
import com.example.android_learning_app.databinding.ActivityAlbumBinding
import com.example.android_learning_app.databinding.ActivityAlbumRecyclerBinding
import com.example.android_learning_app.factory.AlbumFactory
import com.example.android_learning_app.repository.AlbumRepository
import com.example.android_learning_app.viewModels.AlbumViewModel

class AlbumRecyclerActivity : AppCompatActivity() {
    lateinit var binding: ActivityAlbumRecyclerBinding
    lateinit var factory: AlbumFactory
    lateinit var viewModel: AlbumViewModel
    lateinit var adapter: MyAlbumRecyclerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_album_recycler)
        factory = AlbumFactory(AlbumRepository())
        viewModel = ViewModelProvider(this, factory)[AlbumViewModel::class.java]
        binding.lifecycleOwner = this

        binding.recyclerView.layoutManager = LinearLayoutManager(this)

        viewModel.getAlbum().observe(this, Observer {
            adapter = MyAlbumRecyclerViewAdapter(it)
            binding.recyclerView.adapter = adapter
            adapter.notifyDataSetChanged()
        })
    }
}