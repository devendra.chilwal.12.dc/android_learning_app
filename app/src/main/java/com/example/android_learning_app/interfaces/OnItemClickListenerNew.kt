package com.example.android_learning_app.interfaces

import com.example.android_learning_app.roomDb.Employee
import com.example.androidconcept_tp_5.model.Student

interface OnItemClickListenerNew {
    fun onItemClick(position: Int, employee: Employee)
}