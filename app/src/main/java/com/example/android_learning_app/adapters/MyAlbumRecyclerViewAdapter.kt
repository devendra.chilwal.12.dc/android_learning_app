package com.example.android_learning_app.adapters

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.android_learning_app.R
import com.example.android_learning_app.databinding.ItemAlbumBinding
import com.example.android_learning_app.databinding.LayoutItemsBinding
import com.example.android_learning_app.response.Album

class MyAlbumRecyclerViewAdapter(private val albumData: List<Album>) : RecyclerView.Adapter<MyAlbumRecyclerViewAdapter.MyViewHolder>() {

    inner class MyViewHolder(var binding: ItemAlbumBinding): RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        var view: View? = LayoutInflater.from(parent.context).inflate(R.layout.item_album, parent, false)
        var binding: ItemAlbumBinding = DataBindingUtil.bind(view!!)!!
        return MyViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return albumData.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val album = albumData[position]

        holder.binding.apply {
            tvId.text = album.ID.toString()
            tvUserId.text = album.userID.toString()
            tvTitle.text = album.title
        }
    }
}