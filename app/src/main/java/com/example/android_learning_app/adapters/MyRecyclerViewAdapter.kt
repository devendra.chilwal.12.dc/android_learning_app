package com.example.android_learning_app.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.android_learning_app.R
import com.example.android_learning_app.databinding.LayoutItemsBinding
import com.example.android_learning_app.interfaces.OnItemClickListener
import com.example.android_learning_app.interfaces.OnItemClickListenerNew
import com.example.android_learning_app.roomDb.Employee
import com.example.androidconcept_tp_5.model.Student

class MyRecyclerViewAdapter(private val listOfStudent: List<Student>, private val context: Context): RecyclerView.Adapter<MyRecyclerViewAdapter.MyViewHolder>() {

    private lateinit var onItemClickListener: OnItemClickListener
    inner class MyViewHolder(var binding: LayoutItemsBinding): RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        var view: View? = LayoutInflater.from(parent.context).inflate(R.layout.layout_items, parent, false)
        var binding: LayoutItemsBinding = DataBindingUtil.bind(view!!)!!
        return MyViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return listOfStudent.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val student = listOfStudent[position]
//        holder.binding.tvFname.text = student.fName
//        holder.binding.tvLname.text = student.lName
//        holder.binding.tvPhone.text = student.phone

        holder.binding.apply {
            this.tvFname.text = student.fName
            this.tvLname.text = student.lName
            this.tvPhone.text = student.phone
        }

        holder.binding.tvFname.setOnClickListener {
            Toast.makeText(context, holder.binding.tvFname.text, Toast.LENGTH_SHORT).show()
        }

        holder.itemView.setOnClickListener {
//            Toast.makeText(context, ""+listOfStudent[position], Toast.LENGTH_SHORT).show()
            if (onItemClickListener != null){
                onItemClickListener.onItemClick(position, listOfStudent[position])
            }
        }
    }

    fun setOnItemClickListener(onItemClickListener: OnItemClickListener){
        this.onItemClickListener = onItemClickListener
    }
}