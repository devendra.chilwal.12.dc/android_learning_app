package com.example.android_learning_app.adapters

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Base64.*
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.android_learning_app.R
import com.example.android_learning_app.databinding.LayoutItemsBinding
import com.example.android_learning_app.interfaces.OnItemClickListenerNew
import com.example.android_learning_app.roomDb.Employee


class MyRecyclerViewAdapterNew(private val listOfStudent: List<Employee>,
                               private val context: Context, private val onItemClickListenerNew: OnItemClickListenerNew): RecyclerView.Adapter<MyRecyclerViewAdapterNew.MyViewHolder>() {

//    private lateinit var onItemClickListener: OnItemClickListenerNew
    inner class MyViewHolder(var binding: LayoutItemsBinding): RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        var view: View? = LayoutInflater.from(parent.context).inflate(R.layout.layout_items, parent, false)
        var binding: LayoutItemsBinding = DataBindingUtil.bind(view!!)!!
        return MyViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return listOfStudent.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val student = listOfStudent[position]
//        holder.binding.tvFname.text = student.fName
//        holder.binding.tvLname.text = student.lName
//        holder.binding.tvPhone.text = student.phone

        holder.binding.apply {
            this.tvFname.text = student.fName
            this.tvLname.text = student.lName
            this.tvPhone.text = student.phone
            this.imageView.setImageBitmap(convertBase64ToBitmap(student.photo))
        }

        holder.binding.tvFname.setOnClickListener {
            Toast.makeText(context, holder.binding.tvFname.text, Toast.LENGTH_SHORT).show()
        }

        holder.itemView.setOnClickListener {
//            Toast.makeText(context, ""+listOfStudent[position], Toast.LENGTH_SHORT).show()
            if (onItemClickListenerNew != null){
                onItemClickListenerNew.onItemClick(position, listOfStudent[position])
            }
        }
    }

//    fun setOnItemClickListener(onItemClickListener: OnItemClickListenerNew){
//        this.onItemClickListener = onItemClickListener
//    }


    private fun convertBase64ToBitmap(base64Image: String): Bitmap? {
        val decodedByteArray: ByteArray = decode(base64Image, NO_WRAP)
        return BitmapFactory.decodeByteArray(decodedByteArray, 0, decodedByteArray.size)
    }
}