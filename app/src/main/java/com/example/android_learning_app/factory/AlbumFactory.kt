package com.example.android_learning_app.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.android_learning_app.model.DbViewModel
import com.example.android_learning_app.repository.AlbumRepository
import com.example.android_learning_app.viewModels.AlbumViewModel

class AlbumFactory(private val repository: AlbumRepository): ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(AlbumViewModel::class.java)){
            return AlbumViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown class")
    }
}