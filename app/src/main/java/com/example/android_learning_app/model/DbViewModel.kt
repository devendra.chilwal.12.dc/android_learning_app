package com.example.android_learning_app.model

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.android_learning_app.repository.DbRepository
import com.example.android_learning_app.roomDb.Employee
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class DbViewModel(private val repository: DbRepository): ViewModel() {
    suspend fun insertEmployee(employee: Employee){
        repository.insertEmployee(employee)
    }

    val getAllEmployee = repository.getAllEmployee

    fun deleteSingleRecord(employee: Employee){
        viewModelScope.launch(Dispatchers.IO){
            repository.deleteSingleRecord(employee)
        }
    }

    fun updateRecord(employee: Employee){
        viewModelScope.launch(Dispatchers.IO){
            repository.updateRecord(employee)
        }
    }

    fun deleteAllRecord(){
        viewModelScope.launch(Dispatchers.IO){
            repository.deleteAllRecord()
        }
    }
}