package com.example.android_learning_app.model

class Product {
    var id: Int = 0
    var title: String = ""
    var price: Double = 0.0
    var description: String = ""
    var category: String = ""
    var image: String = ""
    var rating: Rating? = null
}