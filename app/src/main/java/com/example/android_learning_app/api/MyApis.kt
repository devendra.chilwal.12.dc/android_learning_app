package com.example.android_learning_app.api

import com.example.android_learning_app.response.AlbumData
import retrofit2.Call
import retrofit2.http.GET

interface MyApis {
    @GET("/albums")
    fun getAlbum(): Call<AlbumData>

    @GET("/products")
//    fun getProduct(): cal
}