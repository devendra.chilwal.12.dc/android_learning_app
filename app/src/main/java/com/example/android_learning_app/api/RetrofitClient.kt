package com.example.android_learning_app.api

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create

//private const val  BASE_URL = "https://jsonplaceholder.typicode.com"
private const val  BASE_URL = "https://fakestoreapi.com"

object RetrofitClient {
    val retrofitClient : Retrofit.Builder by lazy {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY)

        val okHttpClient = OkHttpClient.Builder()
        okHttpClient.addInterceptor(loggingInterceptor)
        okHttpClient.build()


        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            .client(okHttpClient.build())
    }

    val apiInterface: MyApis by lazy {
        retrofitClient.build().create(MyApis::class.java)
    }


}